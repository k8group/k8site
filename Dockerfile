FROM nginx:alpine
COPY ./public /usr/share/nginx/html/
COPY default.conf /etc/nginx/conf.d/default.conf
COPY fastcgi-php.conf /etc/nginx/snippets/fastcgi-php.conf
RUN mkdir -p /var/run/php/
RUN apk add --no-cache --update alpine-sdk bash openrc php7 php7-bcmath php7-ctype php7-curl php7-dom php7-fileinfo php7-fpm php7-gd php7-json php7-mbstring php7-pdo php7-pdo_mysql php7-pecl-redis php7-phar php7-session php7-simplexml php7-tokenizer php7-xml php7-xmlwriter supervisor
COPY www.conf /etc/php7/php-fpm.d/www.conf
RUN chown -R nginx:nginx /usr/share/nginx/html
RUN chown -R nginx:nginx /var/run/php
COPY ./supervisord.conf /etc/
ENTRYPOINT /usr/bin/supervisord -c /etc/supervisord.conf
